# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/08/28 14:28:00 by mmeisson          #+#    #+#              #
#    Updated: 2017/03/18 13:47:28 by mmeisson         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME			= lem-in

CC				= clang

CFLAGS			= -MD -Wall -Werror -Wextra

SRCS_PATHS		= ./srcs/
SRCS			= hash_table.c basics.c lst_str.c main.c parse.c room_connect.c \
				  ft_exit.c get_next_line.c finlout.c algo.c get_options.c \
				  parse_get_rooms.c lem_walk.c lemin.c create_connection.c \
				  hash_bis.c

INCS_PATHS		= ./incs/ ./libft/incs/ ./printf/incs/
INCS			= $(addprefix -I,$(INCS_PATHS))

OBJS_PATH		= ./.objs/
OBJS_NAME		= $(SRCS:.c=.o)
OBJS			= $(addprefix $(OBJS_PATH), $(OBJS_NAME))

DEPS			= $(OBJS:.o=.d)

LIB_PATHS		= ./libft/ ./printf/
LIBS			= $(addprefix -L,$(LIB_PATHS))

LDFLAGS			= $(LIBS) -lft -lftprintf



all: $(NAME)


$(NAME): $(OBJS)
	@$(foreach PATHS, $(LIB_PATHS),\
		echo "# # # # # #\n#";\
		echo '# \033[31m' Compiling $(PATHS) '\033[0m';\
		echo "#\n# # # # # #";\
		make -j8 -C $(PATHS);\
	)
	$(CC) $^ -o $@ $(LDFLAGS)

$(OBJS_PATH)%.o: $(SRCS_PATHS)%.c
	@mkdir -p $(OBJS_PATH)
	$(CC) $(CFLAGS) $(INCS) -o $@ -c $<

clean:
	@$(foreach PATHS, $(LIB_PATHS), make -j8 -C $(PATHS) clean;)
	rm -rf $(OBJS_PATH)

fclean:
	@$(foreach PATHS, $(LIB_PATHS), make -j8 -C $(PATHS) fclean;)
	rm -rf $(OBJS_PATH)
	rm -f $(NAME)

re: fclean all

-include $(DEPS)
