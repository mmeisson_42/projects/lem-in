/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 15:48:42 by mmeisson          #+#    #+#             */
/*   Updated: 2017/03/18 18:18:42 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

bool			massivequ(char *str, char *cmp[])
{
	size_t		i;

	i = 0;
	while (cmp[i] != NULL)
	{
		if (ft_strequ(str, cmp[i]))
			return (true);
		i++;
	}
	return (NULL);
}

static void		walk_through(t_lemin *lem, t_room *tmp)
{
	int			len;
	t_room		*to;

	len = 1;
	to = lem->end;
	while (true)
	{
		tmp->valid = true;
		tmp->tos[0] = to;
		tmp->tos[1] = NULL;
		tmp->len = len++;
		to = tmp;
		if (tmp->from == lem->start)
			break ;
		tmp = tmp->from;
	}
	push_room(&lem->ways, tmp);
}

void			valid_this_way(t_lemin *lem, t_room *this)
{
	t_room		*tmp;

	tmp = this;
	while (tmp != lem->start)
	{
		if (tmp->valid == true)
			return ;
		tmp = tmp->from;
	}
	walk_through(lem, this);
}

static void		expand_file(t_lemin *lem, t_file **file, t_room *curr)
{
	size_t	i;

	i = 0;
	if (curr->tos != NULL)
	{
		while (curr->tos[i] != NULL)
		{
			if (curr->tos[i] == lem->end)
			{
				curr->tos[i]->way = true;
				valid_this_way(lem, curr);
			}
			else if (curr->tos[i]->way == false)
			{
				curr->tos[i]->from = curr;
				curr->tos[i]->way = true;
				push_room(file, curr->tos[i]);
			}
			i++;
		}
	}
}

void			algo(t_lemin *lem)
{
	t_file		*file;
	t_room		*curr;

	file = NULL;
	lem->start->way = true;
	if (lem->end->tos)
		lem->end->tos[0] = NULL;
	push_room(&file, lem->start);
	while (file != NULL)
	{
		curr = pop_room(&file);
		expand_file(lem, &file, curr);
	}
}
