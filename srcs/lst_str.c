/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_str.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 15:49:06 by mmeisson          #+#    #+#             */
/*   Updated: 2017/03/11 15:49:07 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

t_str		*lst_strnew(const char *str)
{
	t_str		*new;

	if (!(new = malloc(sizeof(*new))))
		MALLOC_ERROR();
	if (!(new->str = ft_strdup(str)))
		MALLOC_ERROR();
	new->next = NULL;
	return (new);
}

/*
**	Each new element is inserted in front, so reverse print this
*/

void		lst_stradd(t_str **head, t_str *new)
{
	new->next = *head;
	*head = new;
}

void		lst_strdel(t_str **head)
{
	if (*head != NULL)
	{
		lst_strdel(&(*head)->next);
		free((*head)->str);
		free(*head);
		*head = NULL;
	}
}
