/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_options.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 15:51:37 by mmeisson          #+#    #+#             */
/*   Updated: 2017/03/11 15:51:40 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

static void		usage(const char *name, t_lemin *lem, int exit_value)
{
	ft_printf("Usage: %s ([-help] [-way] [-color]){ < anthill } || [-server]\n",
			name);
	ft_exit(lem, exit_value);
	exit(exit_value);
}

char			*get_options_from_client(t_lemin *lem)
{
	size_t		i;
	uint8_t		*opts;
	char		*str;
	char		**argv;

	i = 0;
	if (get_next_line(0, &str) < 1)
		return (NULL);
	if (str[0] != '-' && ft_isnumber(str))
		return (str);
	argv = ft_strsplit(str, ' ');
	opts = &lem->opts;
	while (argv != NULL && argv[i] != NULL)
	{
		if (ft_strequ(argv[i], "-way"))
			TOGGLE_WAY(*opts);
		else if (ft_strequ(argv[i], "-color"))
			TOGGLE_COLOR(*opts);
		else if (ft_strequ(argv[i], "-help"))
			usage("", lem, 0);
		else
			usage("", lem, 1);
		i++;
	}
	return (NULL);
}

void			get_options(t_lemin *lem, const char *argv[])
{
	size_t		i;
	uint8_t		*opts;

	opts = &lem->opts;
	i = 1;
	while (argv[i] != NULL)
	{
		if (ft_strequ(argv[i], "-server"))
		{
			*opts = 0x0;
			TOGGLE_SERV(*opts);
			return ;
		}
		else if (ft_strequ(argv[i], "-way"))
			TOGGLE_WAY(*opts);
		else if (ft_strequ(argv[i], "-color"))
			TOGGLE_COLOR(*opts);
		else if (ft_strequ(argv[i], "-help"))
			usage(argv[0], lem, 0);
		else
			usage(argv[0], lem, 1);
		i++;
	}
}
