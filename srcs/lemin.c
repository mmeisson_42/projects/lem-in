/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lemin.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 15:49:00 by mmeisson          #+#    #+#             */
/*   Updated: 2017/03/25 18:34:05 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

static void		print_input(t_lemin *lem, t_str **str)
{
	if (*str != NULL)
	{
		print_input(lem, &(*str)->next);
		ft_printf("%s\n", (*str)->str);
		free((*str)->str);
		free(*str);
		*str = NULL;
	}
}

static void		print_ways(t_file *f)
{
	t_room		*r;
	int			i;

	i = 0;
	while (f != NULL)
	{
		ft_printf("Way number %d :\n", i++);
		r = f->room;
		ft_printf("\t\033[035m{ start -");
		while (r != NULL)
		{
			ft_printf(" %s -", r->name);
			r = r->tos[0];
		}
		ft_printf(" end }\033[0m\n");
		f = f->next;
	}
}

int				special(t_lemin *lem)
{
	t_room		**tos;
	size_t		i;

	tos = lem->start->tos;
	i = 0;
	while (tos && tos[i])
	{
		if (tos[i] == lem->end)
		{
			lem->start->tos[0] = lem->end;
			lem->start->tos[1] = NULL;
			lem->end->tos[0] = NULL;
			lem->end->from = lem->start;
			push_room(&lem->ways, lem->end);
			return (1);
		}
		i++;
	}
	return (0);
}

void			lemin(t_lemin lem, bool server_mode)
{
	char		*str;

	str = NULL;
	if (server_mode)
		str = get_options_from_client(&lem);
	parse(&lem, str);
	if (lem.start == NULL || lem.end == NULL || lem.ants < 1)
		ft_exit(&lem, 1);
	if (!special(&lem))
		algo(&lem);
	if (lem.ways == NULL)
		ft_exit(&lem, 1);
	print_input(&lem, &lem.str);
	if (GET_WAY(lem.opts) == true)
		print_ways(lem.ways);
	lem_walk(&lem);
	ft_exit(&lem, 0);
}
