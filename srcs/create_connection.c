/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_connection.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 15:48:17 by mmeisson          #+#    #+#             */
/*   Updated: 2017/03/11 19:50:40 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

int		create_connection(void)
{
	int					sockfd;
	struct protoent		*proto;
	struct sockaddr_in	in;

	in.sin_family = AF_INET;
	in.sin_port = htons(PORT);
	in.sin_addr.s_addr = INADDR_ANY;
	proto = getprotobyname("tcp");
	if (proto == NULL)
	{
		ft_dprintf(2, "Protocol's number not found\n");
		exit(EXIT_FAILURE);
	}
	sockfd = socket(PF_INET, SOCK_STREAM, proto->p_proto);
	if (sockfd == -1)
	{
		ft_dprintf(2, "Can't get a socket\n");
		exit(EXIT_FAILURE);
	}
	if (bind(sockfd, (struct sockaddr *)&in, sizeof(in)) == -1)
	{
		ft_dprintf(2, "Connection can't be etablished\n");
		exit(EXIT_FAILURE);
	}
	return (sockfd);
}
