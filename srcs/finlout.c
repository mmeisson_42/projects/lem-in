/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   finlout.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 15:48:25 by mmeisson          #+#    #+#             */
/*   Updated: 2017/03/11 15:48:27 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

t_room		*pop_room(t_file **head)
{
	t_room		*ret;
	t_file		*tmp;

	ret = NULL;
	if (*head != NULL)
	{
		tmp = *head;
		*head = tmp->next;
		ret = tmp->room;
		free(tmp);
	}
	return (ret);
}

void		push_room(t_file **head, t_room *new)
{
	t_file		*last;
	t_file		*tmp;

	if (!(last = malloc(sizeof(*last))))
		MALLOC_ERROR();
	last->room = new;
	last->next = NULL;
	if (*head == NULL)
	{
		*head = last;
	}
	else
	{
		tmp = *head;
		while (tmp->next != NULL)
			tmp = tmp->next;
		tmp->next = last;
	}
}
