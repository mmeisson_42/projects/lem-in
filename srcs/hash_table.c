/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash_table.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 15:52:28 by mmeisson          #+#    #+#             */
/*   Updated: 2017/03/11 19:41:08 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

size_t				hash_get_key(const char *name)
{
	size_t		i;
	size_t		ret;

	i = 0;
	ret = 0;
	while (name[i] != '\0')
	{
		ret = ret * 37 + (int)name[i];
		i += 1;
	}
	return (ret % TABLE_SIZE);
}

static bool			valid_room(t_room *this, t_room *new)
{
	if (ft_strequ(this->name, new->name))
	{
		ft_strdel(&new->name);
		free(new);
		return (false);
	}
	return (true);
}

static char			*get_name(const char *str)
{
	size_t	len;
	char	*name;

	len = 0;
	while (str[len] != '\0' && str[len] != ' ' && str[len] != '\t')
	{
		len++;
	}
	if (!(name = ft_strndup(str, len)))
		MALLOC_ERROR();
	return (name);
}

/*
**	hash_set_room return 0 if a new room is inserted in table, 1 otherwise
**	hash_set_room doesn't allow multiple room with same name
*/

t_room				*insert_in(t_room **alst, t_room *new)
{
	t_room		*tmp;

	if (*alst == NULL)
	{
		*alst = new;
	}
	else
	{
		tmp = *alst;
		if (valid_room(tmp, new) == false)
			return (NULL);
		while (tmp->next)
		{
			if (valid_room(tmp->next, new) == false)
				return (NULL);
			tmp = tmp->next;
		}
		tmp->next = new;
	}
	return (new);
}

t_room				*hash_set_room(const char *str, t_type type, t_room **table)
{
	size_t		key;
	t_room		*new;
	t_room		*this;
	char		*name;

	name = get_name(str);
	key = hash_get_key(name);
	if (!(new = ft_memalloc(sizeof(*this))))
		MALLOC_ERROR();
	new->type = type;
	new->name = name;
	return (insert_in(&table[key], new));
}
