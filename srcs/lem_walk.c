/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_walk.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 15:48:53 by mmeisson          #+#    #+#             */
/*   Updated: 2017/03/12 17:04:06 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

static const char		*g_colors[19] = {
	"\033[031m",
	"\033[032m",
	"\033[033m",
	"\033[034m",
	"\033[035m",
	"\033[036m",
	"\033[037m",
	"\033[038m",
	"\033[039m",
	"\033[091m",
	"\033[092m",
	"\033[093m",
	"\033[094m",
	"\033[095m",
	"\033[096m",
	"\033[097m",
	"\033[098m",
	"\033[099m",
};

static void		set_color(t_lemin *lem)
{
	t_file	*f;
	int		set;
	t_room	*r;

	set = 0;
	f = lem->ways;
	set = 0;
	while (f != NULL)
	{
		r = f->room;
		while (r != NULL)
		{
			r->color = (char *)g_colors[set % 19];
			r = r->tos[0];
		}
		set++;
		f = f->next;
	}
}

static int		walk_em_all(int ant, t_room *r)
{
	char		*color;
	int			numb;

	while (r != NULL && ant == 0)
	{
		ft_swap(&ant, &r->ant);
		r = r->tos[0];
	}
	numb = 0;
	while (r != NULL && ant != 0)
	{
		if (r->color != NULL)
			color = r->color;
		else
			color = "";
		ft_printf("L%d-%s%s\033[0m ", ant, color, r->name);
		ft_swap(&ant, &r->ant);
		r = r->tos[0];
		numb++;
	}
	return (numb);
}

void			under_walk(t_lemin *lem, t_file *f, int i)
{
	while (42)
	{
		if (f == NULL)
		{
			if (i == 0)
				break ;
			i = 0;
			if (lem->start->tos[0] != lem->end)
				ft_putchar('\n');
			f = lem->ways;
		}
		i += walk_em_all(0, f->room);
		f = f->next;
	}
	ft_putchar('\n');
}

void			lem_walk(t_lemin *lem)
{
	int		i;
	t_file	*f;

	i = 1;
	f = NULL;
	if (GET_COLOR(lem->opts))
		set_color(lem);
	while (i <= lem->ants)
	{
		if (f == NULL)
		{
			if (lem->start->tos[0] != lem->end)
				ft_putchar('\n');
			f = lem->ways;
		}
		walk_em_all(i, f->room);
		f = f->next;
		i++;
	}
	under_walk(lem, f, i);
}
