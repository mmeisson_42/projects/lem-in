/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   room_connect.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 15:49:16 by mmeisson          #+#    #+#             */
/*   Updated: 2017/03/11 15:59:55 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

static void		room_under_connect(t_room *from, t_room *to)
{
	size_t		len;
	t_room		**tos;

	len = 0;
	if (from->tos != NULL)
	{
		while (from->tos[len] != NULL)
			len++;
	}
	if (!(tos = malloc(sizeof(*tos) * (len + 2))))
		MALLOC_ERROR();
	len = 0;
	tos[len] = to;
	if (from->tos != NULL)
	{
		while (from->tos[len] != NULL)
		{
			tos[len + 1] = from->tos[len];
			len++;
		}
	}
	tos[len + 1] = NULL;
	if (from->tos != NULL)
		free(from->tos);
	from->tos = tos;
}

void			room_connect(t_room *f, t_room *s)
{
	room_under_connect(f, s);
	room_under_connect(s, f);
}

void			room_lst_del(t_room **r)
{
	if (*r != NULL)
	{
		room_lst_del(&(*r)->next);
		ft_strdel(&(*r)->name);
		free((*r)->tos);
		ft_memdel((void **)r);
	}
}
