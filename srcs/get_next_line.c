/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpiccolo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/08 04:17:49 by rpiccolo          #+#    #+#             */
/*   Updated: 2017/03/25 19:09:11 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

char		*ft_extract_mylines(char **mylines)
{
	char		*tmp;
	char		*extract;

	if (!*mylines || !**mylines)
	{
		ft_strdel(mylines);
		return (NULL);
	}
	if ((tmp = ft_strchr(*mylines, '\n')))
	{
		*tmp = '\0';
		extract = ft_strdup(*mylines);
		ft_strcpy(*mylines, ++tmp);
		return (extract);
	}
	extract = ft_strdup(*mylines);
	ft_strdel(mylines);
	*mylines = ft_strdup("");
	return (extract);
}

void		ft_join(char **mylines, char *buf)
{
	char			*tmp;

	tmp = NULL;
	if (!*mylines)
		*mylines = ft_strdup("\0");
	tmp = ft_strjoin(*mylines, buf);
	ft_strdel(mylines);
	*mylines = tmp;
}

t_myline	*ft_init(int fd)
{
	t_myline		*tmp;

	tmp = (t_myline*)malloc(sizeof(t_myline));
	tmp->fd = fd;
	tmp->content = ft_strnew(0);
	tmp->next = NULL;
	return (tmp);
}

t_myline	*ft_checkfd(t_myline **mylines, int fd)
{
	t_myline		*tmp;

	tmp = *mylines;
	while (tmp)
	{
		if (tmp->fd == fd)
			return (tmp);
		tmp = tmp->next;
	}
	tmp = ft_init(fd);
	tmp->next = *mylines;
	*mylines = tmp;
	return (tmp);
}

int			get_next_line(const int fd, char **line)
{
	char				buf[BUFF_SIZE + 1];
	int					ret;
	static	t_myline	*stock = NULL;
	t_myline			*mylines;

	if (fd < 0 || BUFF_SIZE < 0 || !line || BUFF_SIZE > MAXINT)
		return (-1);
	mylines = ft_checkfd(&stock, fd);
	while ((ret = read(fd, buf, BUFF_SIZE)) && ret >= 0)
	{
		buf[ret] = '\0';
		ft_join(&mylines->content, buf);
		if (ft_strchr(buf, '\n'))
			break ;
	}
	if (ret < 0)
		return (-1);
	if (!(*line = ft_extract_mylines(&(mylines->content))))
		*line = ft_strnew(0);
	if (*line && mylines->content)
		return (1);
	return (0);
}
