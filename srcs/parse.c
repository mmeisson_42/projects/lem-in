/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 15:49:25 by mmeisson          #+#    #+#             */
/*   Updated: 2017/03/25 19:22:22 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

static bool		parse_get_ants(t_lemin *ret, char *str)
{
	while (1)
	{
		if (str == NULL)
			if (get_next_line(0, &str) <= 0)
				break ;
		if (str[0] == '#' && command(str) == ERROR)
			lst_stradd(&ret->str, lst_strnew(str));
		else if (is_number(str) == false)
		{
			ft_strdel(&str);
			return (false);
		}
		else
		{
			ret->ants = ft_atoi(str);
			lst_stradd(&ret->str, lst_strnew(str));
			ft_strdel(&str);
			return (true);
		}
		ft_strdel(&str);
	}
	return (false);
}

static void		shame_deletion(char **str1, char **str2)
{
	ft_strdel(str1);
	ft_strdel(str2);
}

static void		parse_get_connections(t_lemin *ret, char *str)
{
	char			*tmp;
	ssize_t			ind;
	t_room			*tab[2];

	while (true)
	{
		if (!(tmp = ft_strdup(str)))
			MALLOC_ERROR();
		if (str[0] != '#')
		{
			if ((ind = ft_strchri(str, '-')) == -1)
				break ;
			str[ind] = '\0';
			tab[0] = hash_get_room(str, ret->table);
			tab[1] = hash_get_room(str + ind + 1, ret->table);
			if (tab[0] == NULL || tab[1] == NULL)
				break ;
			room_connect(tab[0], tab[1]);
		}
		lst_stradd(&ret->str, lst_strnew(tmp));
		shame_deletion(&tmp, &str);
		if (get_next_line(0, &str) < 1)
			break ;
	}
	shame_deletion(&tmp, &str);
}

void			parse(t_lemin *ret, char *str)
{
	if (!(ret->table = ft_memalloc(sizeof(t_room *) * TABLE_SIZE)))
		MALLOC_ERROR();
	if (parse_get_ants(ret, str) == false)
		ft_exit(ret, 1);
	if ((str = parse_get_rooms(ret)) == NULL)
		ft_exit(ret, 1);
	parse_get_connections(ret, str);
}
