/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exit.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 15:48:33 by mmeisson          #+#    #+#             */
/*   Updated: 2017/03/11 15:48:34 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

static void		file_del(t_file **lst)
{
	if (*lst != NULL)
	{
		file_del(&(*lst)->next);
		free(*lst);
		*lst = NULL;
	}
}

void			ft_exit(t_lemin *ret, int exit_value)
{
	size_t		i;

	i = 0;
	if (ret->table != NULL)
	{
		while (i < TABLE_SIZE)
		{
			if (ret->table[i] != NULL)
				room_lst_del(ret->table + i);
			i++;
		}
	}
	lst_strdel(&ret->str);
	free(ret->table);
	file_del(&ret->ways);
	if (exit_value != 0)
		ft_printf("ERROR\n");
	exit(exit_value);
}
