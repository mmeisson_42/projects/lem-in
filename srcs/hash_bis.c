/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash_bis.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 19:38:57 by mmeisson          #+#    #+#             */
/*   Updated: 2017/03/11 19:42:12 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

/*
**	hash_get_room return the room associated with given name if found,
**	NULL otherwise
*/

t_room				*hash_get_room(const char *name, t_room **table)
{
	size_t		key;
	t_room		*this;

	key = hash_get_key(name);
	this = table[key];
	while (this != NULL)
	{
		if (ft_strequ(this->name, name))
			break ;
		this = this->next;
	}
	return (this);
}
