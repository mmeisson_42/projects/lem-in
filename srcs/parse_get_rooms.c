/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_get_rooms.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 15:58:03 by mmeisson          #+#    #+#             */
/*   Updated: 2017/03/11 19:27:34 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

static bool		set_start_end(t_type *room, t_lemin *ret, t_room *new)
{
	if (*room == START)
	{
		if (ret->start != NULL)
			return (false);
		ret->start = new;
	}
	else if (*room == END)
	{
		if (ret->end != NULL)
			return (false);
		ret->end = new;
	}
	*room = NORMAL;
	return (true);
}

static void		manage_com(t_type *room, t_lemin *ret, char **str)
{
	if (command(*str) != ERROR)
		*room = command(*str);
	lst_stradd(&ret->str, lst_strnew(*str));
	ft_strdel(str);
}

char			*wrap_strdel(char *str)
{
	ft_strdel(&str);
	return (NULL);
}

char			*parse_get_rooms(t_lemin *ret)
{
	t_type		room;
	t_room		*new;
	char		*str;

	room = NORMAL;
	while (get_next_line(0, &str) > 0)
	{
		if (str[0] == '#')
			manage_com(&room, ret, &str);
		else if (str[0] != '\0')
		{
			if (is_descriptor(str) == false)
				return (str);
			if (((new = hash_set_room(str, room, ret->table)) == NULL)
					|| (set_start_end(&room, ret, new) == false))
				return (wrap_strdel(str));
			lst_stradd(&ret->str, lst_strnew(str));
			ft_strdel(&str);
		}
		else
			return (wrap_strdel(str));
	}
	return (NULL);
}
