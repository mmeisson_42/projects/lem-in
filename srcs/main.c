/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 15:49:32 by mmeisson          #+#    #+#             */
/*   Updated: 2017/03/25 19:51:19 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem.h"

int				connect_to_client(int socket_server)
{
	int					socket_client;
	fd_set				rfds;
	struct sockaddr_in	in;
	static socklen_t	slen = sizeof(in);
	char				*str;

	FD_ZERO(&rfds);
	FD_SET(0, &rfds);
	FD_SET(socket_server, &rfds);
	while (1)
	{
		select(socket_server + 1, &rfds, NULL, NULL, NULL);
		if (FD_ISSET(0, &rfds) && get_next_line(0, &str) == 0)
		{
			ft_strdel(&str);
			return (-1);
		}
		ft_strdel(&str);
		if (FD_ISSET(socket_server, &rfds))
		{
			if ((socket_client =
					accept(socket_server, (struct sockaddr *)&in, &slen)) != -1)
				return (socket_client);
		}
	}
}

void			start_listening(t_lemin lem, int socket_client)
{
	dup2(socket_client, 0);
	dup2(socket_client, 1);
	lemin(lem, true);
	close(socket_client);
	exit(0);
}

int				main(int argc, const char *argv[])
{
	t_lemin		lem;
	int			socket_server;
	int			socket_client;

	argc = 0;
	ft_bzero(&lem, sizeof(lem));
	get_options(&lem, argv);
	if (GET_SERV(lem.opts) == false)
	{
		lemin(lem, false);
		return (0);
	}
	socket_server = create_connection();
	listen(socket_server, 5);
	while ((socket_client = connect_to_client(socket_server)) != -1)
	{
		if (fork() == 0)
		{
			start_listening(lem, socket_client);
		}
		close(socket_client);
	}
	wait(NULL);
	close(socket_server);
	return (0);
}
