/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpiccolo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/08 03:29:46 by rpiccolo          #+#    #+#             */
/*   Updated: 2016/04/14 04:44:28 by rpiccolo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include "libft.h"

# define BUFF_SIZE 32
# define MAXINT 2147483647

typedef struct		s_myline
{
	int				fd;
	char			*content;
	struct s_myline	*next;
}					t_myline;

int					get_next_line(int const fd, char **line);

#endif
