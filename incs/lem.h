/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/28 15:27:30 by mmeisson          #+#    #+#             */
/*   Updated: 2017/03/11 19:41:48 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_H
# define LEM_H

# define MALLOC_ERROR() exit(ft_printf("Error : ENOMEM\n"))

# include <sys/types.h>
# include <sys/socket.h>
# include <sys/select.h>
# include <sys/wait.h>
# include <sys/time.h>
# include <sys/types.h>
# include <netdb.h>
# include <unistd.h>
# include <stdbool.h>
# include <fcntl.h>
# include <stdint.h>
# include "ft_stdio.h"
# include "libft.h"
# include "get_next_line.h"

# define TABLE_SIZE 1024

# define TOGGLE_WAY(opt) opt |= 1 << 0
# define GET_WAY(opt) ((opt >> 0) & 1 ? true : false)

# define TOGGLE_COLOR(opt) opt |= 1 << 1
# define GET_COLOR(opt) ((opt >> 1) & 1 ? true : false)

# define TOGGLE_SERV(opt) opt |= 1 << 2
# define GET_SERV(opt) ((opt >> 2) & 1 ? true : false)

# define PORT 3006

typedef enum	e_type
{
	START,
	END,
	NORMAL,
	ERROR,
}				t_type;

typedef struct	s_room
{
	char			*name;
	t_type			type;
	bool			way;
	bool			valid;
	int				len;
	int				ant;
	char			*color;
	struct s_room	*from;
	struct s_room	**tos;
	struct s_room	*next;
}				t_room;

typedef struct	s_str
{
	char			*str;
	struct s_str	*next;
}				t_str;

struct s_room;

typedef struct	s_file
{
	struct s_room	*room;
	struct s_file	*next;
}				t_file;

typedef struct	s_lemin
{
	t_room		**table;
	t_room		*start;
	t_room		*end;
	int			ants;
	t_str		*str;
	t_file		*ways;
	uint8_t		opts;
}				t_lemin;

/*
**	basics.c
*/
t_type			command(const char *str);
bool			is_number(const char *str);
bool			is_descriptor(const char *str);

/*
**	hash_table.c
*/
size_t			hash_get_key(const char *name);
t_room			*hash_get_room(const char *name, t_room **table);
t_room			*hash_set_room(const char *name, t_type type, t_room **table);

/*
**	lst_str.c
*/
t_str			*lst_strnew(const char *str);
void			lst_stradd(t_str **head, t_str *new);
void			lst_strdel(t_str **head);

/*
**	parse.c
*/
void			parse(t_lemin *ret, char *str);

/*
**	parse_get_rooms.c
*/
char			*parse_get_rooms(t_lemin *ret);

/*
**	room_connect.c
*/
void			room_connect(t_room *f, t_room *s);
void			room_lst_del(t_room **r);

/*
**	ft_exit.c
*/
void			ft_exit(t_lemin *ret, int exit_value);

/*
**	finlout.c
*/
t_room			*pop_room(t_file **head);
void			push_room(t_file **head, t_room *new);

/*
**	algo.c
*/
void			algo(t_lemin *lem);

/*
**	get_options.c
*/
void			get_options(t_lemin *lem, const char *argv[]);
char			*get_options_from_client(t_lemin *lem);

/*
**	lem_walk.c
*/
void			lem_walk(t_lemin *lem);

/*
**	lemin.c
*/
void			lemin(t_lemin lemin, bool server_mode);

/*
**	create_connection.c
*/
int				create_connection(void);

#endif
