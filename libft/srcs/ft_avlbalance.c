/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_avlbalance.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/28 16:32:16 by mmeisson          #+#    #+#             */
/*   Updated: 2017/03/11 19:52:05 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int			ft_avldiff(t_avl *node)
{
	int		diff;

	diff = 0;
	if (node)
	{
		diff = (int)ft_avlheight(node->left);
		diff -= (int)ft_avlheight(node->right);
	}
	return (diff);
}

void				ft_avlbalance(t_avl **tree)
{
	int			diff;

	if (*tree != NULL)
	{
		ft_avlbalance(&(*tree)->right);
		ft_avlbalance(&(*tree)->left);
		diff = ft_avldiff(*tree);
		if (diff > 1)
		{
			if (ft_avldiff((*tree)->left) < 0)
				ft_avlrotatelr(tree);
			else
				ft_avlrotatell(tree);
		}
		else if (diff < -1)
		{
			if (ft_avldiff((*tree)->left) > 0)
				ft_avlrotaterl(tree);
			else
				ft_avlrotaterr(tree);
		}
	}
}
