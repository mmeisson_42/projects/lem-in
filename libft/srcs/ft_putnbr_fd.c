/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 11:01:31 by mmeisson          #+#    #+#             */
/*   Updated: 2015/11/23 11:01:33 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr_fd(int n, int fd)
{
	unsigned int	o;

	o = (n > 0) ? n : -n;
	if (n < 0)
		ft_putchar_fd('-', fd);
	if (o > 9)
		ft_putnbr_fd(o / 10, fd);
	ft_putchar_fd(o % 10 + 48, fd);
}
