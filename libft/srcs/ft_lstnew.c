/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 10:59:12 by mmeisson          #+#    #+#             */
/*   Updated: 2015/12/15 15:38:15 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstnew(const void *content, size_t content_size)
{
	t_list	*node;
	void	*copy;

	if (!(node = ft_memalloc(sizeof(t_list))))
		return (NULL);
	if (content != NULL && content_size != 0)
	{
		if (!(copy = ft_memalloc(content_size)))
		{
			free(node);
			return (NULL);
		}
		ft_memcpy(copy, content, content_size);
		node->content = copy;
		node->content_size = content_size;
	}
	return (node);
}
